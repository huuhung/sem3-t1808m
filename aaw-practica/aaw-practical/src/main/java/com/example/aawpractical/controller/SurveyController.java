package com.example.aawpractical.controller;

import com.example.aawpractical.model.Survey;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.validation.Valid;
import java.util.ArrayList;

@Controller
public class SurveyController {
    @RequestMapping(path = "/", method = RequestMethod.GET)
    public String fetchSurvey(Survey survey, Model model) {

        ArrayList<String> products = new ArrayList<String>();
        products.add("Iphone XS");
        products.add("Iphone XS Max");

        model.addAttribute("products", products);
        return "survey-form";
    }

    @RequestMapping(path = "/", method = RequestMethod.POST)
    public String submitSurvey(@Valid Survey survey, BindingResult result, Model model) {

        if (result.hasErrors()) {
            ArrayList<String> products = new ArrayList<String>();
            products.add("Iphone XS");
            products.add("Iphone XS Max");

            model.addAttribute("products", products);
            return "survey-form";
        }

        model.addAttribute("survey", survey);
        return "survey-result";
    }
}
